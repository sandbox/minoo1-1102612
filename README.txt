// $Id:

Readme file for the page_style for Drupal
---------------------------------------------

page_style.module allows user to specify its own page styles. That mean he can create som kind of style with CSS class. 
After creating a list of CSS classes, user is able to assign page_styles to any created node.
CSS class will be appended to body_classes variable, or to any other user predefined variable.


Installation:
  Installation is like with all normal drupal modules:
  extract the 'page_style' folder from the tar ball to the
  modules directory from your website (typically sites/all/modules).

Configuration:
  The configuration page is at admin/build/page_style,
  where you can configure the page_style module and create new page_styles.
  After creating page_style, you can assign the style in node edit form.