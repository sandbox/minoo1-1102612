<?php
// $Id$
/**
* @file
* Administration page callbacks for the page_style module.
*/

/**
 * Display form for adding and editing page_styles.
 *
 * @ingroup forms
 * @see page_style_form_submit()
 */
function page_style_form(&$form_state, $edit = array()) {
  if (!isset($edit['module'])) {
    $edit['module'] = 'page_style';
  }
//   $edit += array(
//     'name' => '',
//     'description' => '',
//     'help' => '',
//     'nodes' => array(),
//     'hierarchy' => 0,
//     'relations' => 0,
//     'tags' => 0,
//     'multiple' => 0,
//     'required' => 0,
//     'weight' => 0,
//   );
  $form['identification'] = array(
    '#type' => 'fieldset',
    '#title' => t('Identification'),
    '#collapsible' => TRUE,
  );
  $form['identification']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Page style name'),
    '#default_value' => $edit['name'],
    '#maxlength' => 255,
    '#description' => t('The name of Page style.'),
    '#required' => TRUE,
  );
  $form['identification']['css_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of CSS class'),
    '#maxlength' => 255,
    '#default_value' => $edit['css_class'],
    '#description' => t('Name of CSS class added for page'),
    '#required' => TRUE,
  );
  $form['identification']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $edit['description'],
    '#description' => t('Description of the Page style.'),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  if (isset($edit['psid'])) {
    $form['psid'] = array('#type' => 'value', '#value' => $edit['psid']);
    $form['module'] = array('#type' => 'value', '#value' => $edit['module']);
  }
  return $form;
}

/**
 * Accept the form submission for a page_style and save the results.
 */
function page_style_form_submit($form, &$form_state) {
  switch (page_style_save_style($form_state['values'])) {
    case SAVED_NEW:
      drupal_set_message(t('Created new page style %name.', array('%name' => $form_state['values']['name'])));
      watchdog('page_style', 'Created new page style %name.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE, l(t('edit'), 'admin/build/page_style/edit/style/'. $form_state['values']['id']));
      break;
    case SAVED_UPDATED:
      drupal_set_message(t('Updated page style %name.', array('%name' => $form_state['values']['name'])));
      watchdog('page_style', 'Updated page style %name.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE, l(t('edit'), 'admin/build/page_style/edit/style/'. $form_state['values']['id']));
      break;
  }
  $form_state['psid'] = $form_state['values']['psid'];
  $form_state['redirect'] = 'admin/build/page_style';
  return;
}

/**
 * Validation handler for the page_style edit form.
 *
 * @see page_style_form()
 */
function page_style_form_validate($form, &$form_state) {
  if (isset($form_state['values']['weight']) && !is_numeric($form_state['values']['weight'])) {
    form_set_error('weight', t('Weight value must be numeric.'));
  }
  if (isset($form_state['values']['css_class']) && !preg_match('/^[a-zA-Z0-9_-]{1,}$/', $form_state['values']['css_class'])) {
   form_set_error('css_class', t('The CSS class name %css_class is invalid. The name must include only letters, numbers, dashes and underscores.', array('%css_class' => $form_state['values']['css_class'])));
  }
}

/**
 * Display form for adding and editing page_styles.
 *
 * @ingroup forms
 * @see page_style_form_submit()
 */
function page_style_delete_form(&$form_state, $edit = array()) {
  if (isset($edit['psid'])) {
    $psid = $edit['psid'];
    $style = page_style_get_style($psid);
    $form['psid'] = array('#type' => 'value', '#value' => $psid);
    return confirm_form($form,
      t('Are you sure you want to delete the page style %title?',
      array('%title' => $style->name)),
      'admin/build/page_style',
      t('Deleting a page style will remove styles from assigned nodes. This action cannot be undone.'),
      t('Delete'),
      t('Cancel')
    );
  }
}

/**
 * Submit handler for page_style delete form. Deletes page_style.
 *
 * @see page_style_delete_form()
 */
function page_style_delete_form_submit($form, &$form_state) {
  $psid = $form_state['values']['psid'];
  page_style_delete_style($psid);
  $form_state['redirect'] = 'admin/build/page_style';
}

/**
 * Page to edit a page_style.
 */
function page_style_form_edit($psid) {
  $style = page_style_get_style($psid);
  if ($style) {
    return drupal_get_form('page_style_form', (array)$style);
  }
  else {
    return drupal_not_found();
  }
}

/**
 * Form builder to list and manage page_styles.
 *
 * @ingroup forms
 * @see page_style_overview_styles_submit()
 * @see theme_page_style_overview_styles()
 */
function page_style_overview_styles() {
  $styles = page_style_get_styles();
  $form = array('#tree' => TRUE);
  foreach ($styles as $style) {
    $form[$style->psid]['#style'] = (array)$style;
    $form[$style->psid]['name'] = array('#value' => check_plain($style->name));
    $form[$style->psid]['weight'] = array('#type' => 'weight', '#delta' => 10, '#default_value' => $style->weight);
    $form[$style->psid]['css_class'] = array('#value' => check_plain($style->css_class));
    $form[$style->psid]['edit'] = array('#value' => l(t('Edit'), "admin/build/page_style/edit/style/$style->psid"));
    $form[$style->psid]['delete'] = array('#value' => l(t('Delete'), "admin/build/page_style/delete/style/$style->psid"));
  }

  if (count($styles) > 1) {
    $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  }
  elseif (isset($style)) {
    unset($form[$style->psid]['weight']);
  }
  return $form;
}

/**
 * Submit handler for page_style overview. Updates changed page_style weights.
 *
 * @see page_style_overview_styles()
 */
function page_style_overview_styles_submit($form, &$form_state) {
  foreach ($form_state['values'] as $psid => $style) {
    if (is_numeric($psid) && $form[$psid]['#style']['weight'] != $form_state['values'][$psid]['weight']) {
      $form[$psid]['#style']['weight'] = $form_state['values'][$psid]['weight'];
      page_style_save_style($form[$psid]['#style']);
    }
  }
}

/**
 * Theme the page_style overview as a sortable list of vocabularies.
 *
 * @ingroup themeable
 * @see page_style_overview_styles()
 */
function theme_page_style_overview_styles($form) {
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['name'])) {
      $style = &$form[$key];
      $row = array();
      $row[] = drupal_render($style['name']);
      $row[] = drupal_render($style['css_class']);
      if (isset($style['weight'])) {
        $style['weight']['#attributes']['class'] = 'page_style-weight';
        $row[] = drupal_render($style['weight']);
      }
      $row[] = drupal_render($style['edit']);
      $row[] = drupal_render($style['delete']);
      $rows[] = array('data' => $row, 'class' => 'draggable');
    }
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No page styles available.'), 'colspan' => '4'));
  }

  $header = array(t('Name'), t('Name of CSS class'));
  if (isset($form['submit'])) {
    $header[] = t('Weight');
    drupal_add_tabledrag('page_style', 'order', 'sibling', 'page_style-weight');
  }
  $header[] = array('data' => t('Operations'), 'colspan' => '2');
  return theme('table', $header, $rows, array('id' => 'page_style')) . drupal_render($form);
}


/**
* Form builder. Configure page_style module.
*
* @ingroup forms
*/
function page_style_settings() {
  $form['page_style_used_variable'] = array(
    '#type' => 'textfield',
    '#title' => t('Used variable'),
    '#default_value' => variable_get('page_style_used_variable', ''),
    '#description' => t('Users can specify variable, where should be added class_name, if empty, class name will be appended to body_classes variable. This setting can come handy if you are using some theme like Garland'),
  );
  $options = array(
    'edit' => 'Edit of node',
    'preview' => 'Preview of node',
  );
  $form['page_style_display'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display edit of node style'),
    '#options' => $options,
    '#default_value' => variable_get('page_style_display', array('edit' => 0, 'preview' => 0,)),
    '#description' => t('If selected Edit of node, page style of node can be set in editing part of node. If selected Preview of node, page style of node can be set in preview of node.'),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

/**
 * Submit handler for page_style sttings.
 *
 * @see page_style_settings()
 */
function page_style_settings_submit($form, &$form_state) {
  variable_set('page_style_used_variable',$form_state['values']['page_style_used_variable']);
  variable_set('page_style_display',$form_state['values']['page_style_display']);
}

/**
 * Validation handler for the page_style settings form.
 *
 * @see page_style_form()
 */
function page_style_settings_validate($form, &$form_state) {
  if (isset($form_state['values']['page_style_used_variable'])) {
    if ($form_state['values']['page_style_used_variable'] != '' && !preg_match('/^[a-zA-Z0-9_-]{1,}$/', $form_state['values']['page_style_used_variable'])) {
      form_set_error('page_style_used_variable', t('The Used variable %page_style_used_variable is invalid. The name must include only letters, numbers, dashes and underscores.', array('%page_style_used_variable' => $form_state['values']['page_style_used_variable'])));
    }
    if ($form_state['values']['page_style_used_variable'] == 'body_classes') {
      form_set_error('page_style_used_variable', t('Used variable can not be set to body_classes. If you want to append class to variable body_classes leave empty'));
    }
  }
}